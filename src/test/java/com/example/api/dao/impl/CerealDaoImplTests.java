package com.example.api.dao.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import com.example.api.dao.CerealDao;

@ExtendWith(MockitoExtension.class)
class CerealDaoImplTests {

    @Mock
    JdbcTemplate jdbcTemplate;

    @Test
    void testGetMfrById() {
        String sql = "select name from cereal where id = ?";
        Integer id = 1;

        when(jdbcTemplate.queryForObject(sql, String.class, id)).thenReturn("test");
        CerealDao cerealDao = new CerealDaoImpl(jdbcTemplate);

        assertEquals("test", cerealDao.getMfrById(id));
    }

}
