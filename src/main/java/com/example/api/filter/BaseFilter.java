package com.example.api.filter;

import org.springframework.http.HttpStatus;
import org.springframework.web.filter.GenericFilterBean;

import com.example.api.model.ErrorSchema;
import com.example.api.util.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;

public abstract class BaseFilter extends GenericFilterBean {

    @SneakyThrows
    protected void generateNotAllowedResponse(HttpServletResponse resp) {
        ErrorSchema errorSchema = Helper.setNotAllowedMessage();
        ObjectMapper objMapper = new ObjectMapper();

        String body = objMapper.writeValueAsString(errorSchema);

        resp.getOutputStream().write(body.getBytes());
        resp.setContentType("Application/json");
        resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @SneakyThrows
    protected void generateGeneralErrorResponse(HttpServletResponse resp) {
        ErrorSchema errorSchema = Helper.setGeneralErrorMessage();
        ObjectMapper objMapper = new ObjectMapper();

        String body = objMapper.writeValueAsString(errorSchema);

        resp.getOutputStream().write(body.getBytes());
        resp.setContentType("Application/json");
        resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

}
