package com.example.api.filter;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
@Order(1)
public class ClientIdFilter extends BaseFilter {

    @Value("${eai.clientid}")
    private String clientIdEai;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        try {
            if (req.getHeader("client-id") == null ||
                    !req.getHeader("client-id").equalsIgnoreCase(clientIdEai)) {
                this.generateNotAllowedResponse(resp);
                return;
            }
            chain.doFilter(request, response);

        } catch (Exception e) {
            this.generateGeneralErrorResponse(resp);
        }

    }
}
