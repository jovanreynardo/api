package com.example.api.dao;

import java.util.List;

import com.example.api.entity.Cereal;

public interface CerealDao {
    public Cereal getCerealByName(String name);
    public String getMfrById(Integer id);
    public List<Cereal> getAllCereal();
}
