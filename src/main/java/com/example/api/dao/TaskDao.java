package com.example.api.dao;

import java.util.List;

import com.example.api.entity.Task;

public interface TaskDao {
    public List<Task> getAllTasksByEmployeeId(String employeeId);
    public boolean addNewTask(Task task);
}
