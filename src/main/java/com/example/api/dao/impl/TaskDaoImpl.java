package com.example.api.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.example.api.dao.TaskDao;
import com.example.api.entity.Task;

public class TaskDaoImpl implements TaskDao {

    private final JdbcTemplate jdbcTemplate;

    public TaskDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Task> getAllTasksByEmployeeId(String employeeId) {
        String sql = "select * from task where employee_id = ?";
        return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Task.class), employeeId);
    }

    @Override
    public boolean addNewTask(Task task) {
        String sql = "insert into task values (?,?,?,?,?,?)";
        int insert = jdbcTemplate.update(sql, task.getTaskId(), task.getEmployeeId(), task.getTaskName(),
                task.getTaskDueDate(),
                task.getDescription(), task.getStatus());

        return (insert > 0);
    }

}
