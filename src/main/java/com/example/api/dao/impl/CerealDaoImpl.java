package com.example.api.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.api.dao.CerealDao;
import com.example.api.entity.Cereal;

@Repository
public class CerealDaoImpl implements CerealDao{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CerealDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Cereal getCerealByName(String name) {
            String sql = "SELECT * FROM cereal WHERE name = ?";
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Cereal.class), name);
    }

    @Override
    public String getMfrById(Integer id) {
        String sql = "select name from cereal where id = ?";
        return jdbcTemplate.queryForObject(sql, String.class, id);
    }

    @Override
    public List<Cereal> getAllCereal() {
        String sql = "select * from cereal";
        return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Cereal.class));   
    }
    
}
