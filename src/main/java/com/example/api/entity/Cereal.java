package com.example.api.entity;

import lombok.Data;

@Data
public class Cereal {
    private String name;
    private String manufacturer;
    private String type;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;
    private float fiber;
    private float carbo;
    private int sugars;
    private int potass;
    private int vitamins;
    private int shelf;
    private float weight;
    private float cups;
    private float rating;
}
