package com.example.api.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Builder;
import lombok.Data;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Data
@Builder
public class Employee {
    private String employeeId;
    private String employeeName;
    private String phoneNumber;
    private String departmentName;
    private String jobTitle;
    private String email;
    private String salary;
    private String hireDate;
    private String jobId;
}
