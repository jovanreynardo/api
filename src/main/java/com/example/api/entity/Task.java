package com.example.api.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Task{
    private String employeeId;
    private String taskId;
    private String taskName;
    private String taskDueDate;
    private String description;
    private String status;
    private String dayLeft;
    private String remainingTime;
    private String overdue;
}
