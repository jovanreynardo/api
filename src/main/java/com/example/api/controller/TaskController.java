package com.example.api.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.api.entity.Task;
import com.example.api.model.ResponseMessage;
import com.example.api.util.Helper;

@RestController
@RequestMapping("/task")
public class TaskController {

    private static final Logger logger = LogManager.getLogger(TaskController.class);
    static List<Task> taskList = new ArrayList<>();

    @GetMapping
    public ResponseMessage<List<Task>> getAllTasks(@RequestHeader("client-id") String clientId) {
        logger.info("Start Get All Tasks by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();

        List<Task> filteredTaskList = taskList.stream().toList();

        setDayRemainingOverdue(filteredTaskList);

        ResponseMessage<List<Task>> resp = ResponseMessage.<List<Task>>builder().build();

        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(filteredTaskList);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Get All Tasks execute on: {} millis", execTime);
        return resp;
    }

    @GetMapping("/{employee-id}")
    public ResponseMessage<List<Task>> getTaskByEmployeeId(@RequestHeader("client-id") String clientId,
            @PathVariable("employee-id") String employeeId) {
        logger.info("Start Get Tasks by Employee ID. Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();

        List<Task> filteredTaskList = taskList.stream().filter(t -> t.getEmployeeId().equals(employeeId))
                .toList();

        setDayRemainingOverdue(filteredTaskList);

        ResponseMessage<List<Task>> resp = ResponseMessage.<List<Task>>builder().build();

        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(filteredTaskList);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Get Tasks By Employee ID. execute on: {} millis", execTime);
        return resp;
    }

    @PostMapping
    public ResponseMessage<Task> addNewTask(@RequestHeader("client-id") String clientId, @RequestBody Task task) {
        logger.info("Start Add New Task by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();

        ResponseMessage<Task> resp = ResponseMessage.<Task>builder().build();
        UUID uuid = UUID.randomUUID();
        task.setTaskId(uuid.toString());
        taskList.add(task);

        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(task);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Add New Task execute on: {} millis", execTime);

        return resp;
    }

    private void setDayRemainingOverdue(List<Task> tasks) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        for (Task task : tasks) {
            try {
                Date dueDate = sdf.parse(task.getTaskDueDate());
                Date currentDate = new Date();
                long timeDiff = dueDate.getTime() - currentDate.getTime();
                long daysDiff = TimeUnit.MILLISECONDS.toDays(timeDiff);
                long hoursDiff = TimeUnit.MILLISECONDS.toHours(timeDiff) % 24;
                long minutesDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiff) % 60;

                if (daysDiff < 1 && minutesDiff < 0) {
                    task.setOverdue(
                            daysDiff * -1 + " days " + hoursDiff * -1 + " hours " + minutesDiff * -1 + " minutes");
                } else if (daysDiff < 1 && minutesDiff > 0) {
                    task.setRemainingTime(hoursDiff + " hours " + minutesDiff + " minutes");
                } else {
                    task.setDayLeft(daysDiff + " days left");
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

}
