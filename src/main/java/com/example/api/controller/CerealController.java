package com.example.api.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.api.entity.Cereal;
import com.example.api.model.ResponseMessage;
import com.example.api.service.CerealService;
import com.example.api.util.Helper;

@RestController
@RequestMapping(value = "/cereal")
public class CerealController {

    private final CerealService cerealService;
    private static final Logger logger = LogManager.getLogger(CerealController.class);

    @Autowired
    public CerealController(CerealService cerealService) {
        this.cerealService = cerealService;
    }

    @GetMapping("/{name}")
    public ResponseMessage<Cereal> getCereal(@RequestHeader("client-id") String clientId,
            @PathVariable("name") String name) {
        logger.info("Start Get Cereal by Name. Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();

        ResponseMessage<Cereal> resp = ResponseMessage.<Cereal>builder().build();

        Cereal cereal = cerealService.getCerealByName(name);
        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(cereal);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Get Cereal by Name. execute on: {} millis", execTime);

        return resp;
    }

    @GetMapping
    public ResponseMessage<List<Cereal>> getAllCereal(@RequestHeader("client-id") String clientId) {
        logger.info("Start Get All Cereal. Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();

        List<Cereal> cerealList = cerealService.getAllCereal();
        ResponseMessage<List<Cereal>> resp = ResponseMessage.<List<Cereal>>builder().build();

        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(cerealList);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Get All Cereal. execute on: {} millis", execTime);
        return resp;
    }

}
