package com.example.api.controller;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.api.entity.Employee;
import com.example.api.model.RequestEmployee;
import com.example.api.model.ResponseMessage;
import com.example.api.util.Helper;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    private static final Logger logger = LogManager.getLogger(EmployeeController.class);
    static ArrayList<Employee> employeeList = new ArrayList<>();

    @GetMapping("/test")
    public String testMapping(@RequestHeader("client-id") String clientId) {
        logger.info("Test clientId: {}", clientId);
        return clientId;
    }


    @GetMapping
    public ResponseMessage<ArrayList<Employee>> getAllEmployee(@RequestHeader("client-id") String clientId) {
        logger.info("Start Inquiry All by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();
        
        ResponseMessage<ArrayList<Employee>> resp = ResponseMessage.<ArrayList<Employee>>builder().build();

        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(employeeList);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Inquiry All Employee ID execute on: {} millis", execTime);
        return resp;
    }

    @GetMapping("/{employee-id}")
    public ResponseMessage<Employee> getEmployeeById(@RequestHeader("client-id") String clientId,
            @PathVariable("employee-id") String employeeId) {
        logger.info("Start Inquiry Employee by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();

        ResponseMessage<Employee> resp = ResponseMessage.<Employee>builder().build();

        for (Employee employee : employeeList) {
            if (employee.getEmployeeId().equals(employeeId)) {
                resp.setErrorSchema(Helper.setSuccessMessage());
                resp.setOutputSchema(employee);
                long execTime = System.currentTimeMillis() - start;
                logger.info("End Inquiry Employee execute on: {} millis", execTime);

                return resp;
            }
        }

        resp.setErrorSchema(Helper.setNotFoundMessage());
        long execTime = System.currentTimeMillis() - start;
        logger.info("End Inquiry Employee by ID execute on: {} millis", execTime);

        return resp;
    }

    @PostMapping
    public ResponseMessage<Employee> addNewEmployee(@RequestHeader("client-id") String clientId,
            @RequestBody Employee employee) {
        logger.info("Start Add New Employee by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();
        
        ResponseMessage<Employee> resp = ResponseMessage.<Employee>builder().build();

        for (Employee e : employeeList) {
            if(e.getEmployeeId().equals(employee.getEmployeeId())){
                resp.setErrorSchema(Helper.setDuplicateIdMessage());
                resp.setOutputSchema(employee);
                
                long execTime = System.currentTimeMillis() - start;
                logger.info("End Add New Employee execute on: {} millis", execTime);
                return resp;
            }
        }

        employeeList.add(employee);

        resp.setErrorSchema(Helper.setSuccessMessage());
        resp.setOutputSchema(employee);

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Add New Employee execute on: {} millis", execTime);
        return resp;
    }

    @DeleteMapping("/{employee-id}")
    public ResponseMessage<Employee> deleteEmployeeById(@RequestHeader("client-id") String clientId,
            @PathVariable("employee-id") String employeeId) {
        logger.info("Start Delete Employee by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();
        ResponseMessage<Employee> resp = ResponseMessage.<Employee>builder().build();

        for (Employee employee : employeeList) {
            if (employee.getEmployeeId().equals(employeeId)) {
                resp.setErrorSchema(Helper.setSuccessMessage());
                resp.setOutputSchema(employee);

                employeeList.remove(employee);
                return resp;
            }
        }

        resp.setErrorSchema(Helper.setNotFoundMessage());

        long execTime = System.currentTimeMillis() - start;
        logger.info("End Delete Employee execute on: {} millis", execTime);
        return resp;
    }

    @PostMapping("/test-error-code")
    public String testErrorCode(@RequestHeader("client-id") String clientId,
            @RequestBody ResponseMessage<RequestEmployee> request) {
        logger.info("Start test error code by Client ID [ {} ]", clientId);
        long start = System.currentTimeMillis();
        
        
        if (!request.getErrorSchema().getErrorCode().equals("ESB-00-000")) {
            logger.error("FAILED");
            return "FAILED";
        }
        for (Employee e : employeeList) {
            if(e.getEmployeeId().equals(request.getOutputSchema().getEmployeeId())){
                logger.info("Employee Name: {}", e.getEmployeeName());
                logger.info("Hire Date: {}", e.getHireDate());
                logger.info("Status: {}", request.getOutputSchema().getStatus());

                return e.getEmployeeName();
            }
        }

        long execTime = System.currentTimeMillis() - start;
        logger.info("End test error code execute on: {} millis", execTime);
        return "Not Found";
    }

}
