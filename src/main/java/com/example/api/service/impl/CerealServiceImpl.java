package com.example.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dao.CerealDao;
import com.example.api.entity.Cereal;
import com.example.api.service.CerealService;

@Service
public class CerealServiceImpl implements CerealService {

    private final CerealDao cerealDao;
    
    @Autowired
    public CerealServiceImpl(CerealDao cerealDao) {
        this.cerealDao = cerealDao;
    }
    
    @Override
    public Cereal getCerealByName(String name) {
        return cerealDao.getCerealByName(name);
    }

    @Override
    public String getMfrById(Integer id) {
        return cerealDao.getMfrById(id);
    }

    @Override
    public List<Cereal> getAllCereal() {
        return cerealDao.getAllCereal();
    }
}
