package com.example.api.service;

import com.example.api.entity.Task;

public interface TaskService {
    public boolean addNewTask(Task task);
}
