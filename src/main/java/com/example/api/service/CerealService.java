package com.example.api.service;

import java.util.List;

import com.example.api.entity.Cereal;

public interface CerealService {
    public Cereal getCerealByName(String name);
    public String getMfrById(Integer id);
    public List<Cereal> getAllCereal();
}
