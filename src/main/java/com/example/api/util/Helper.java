package com.example.api.util;

import com.example.api.model.ErrorMessage;
import com.example.api.model.ErrorSchema;

public class Helper {

    private Helper() {
        throw new IllegalStateException();
    }

    public static ErrorSchema setSuccessMessage() {
        ErrorSchema errorSchema = ErrorSchema.builder().build();
        ErrorMessage errorMessage = ErrorMessage.builder().build();

        errorMessage.setEnglish("Berhasil");
        errorMessage.setIndonesian("Success");
        errorSchema.setErrorCode("ESB-00-000");
        errorSchema.setErrorMessage(errorMessage);

        return errorSchema;
    }

    public static ErrorSchema setNotFoundMessage() {
        ErrorSchema errorSchema = ErrorSchema.builder().build();
        ErrorMessage errorMessage = ErrorMessage.builder().build();
        errorMessage.setEnglish("Not Found");
        errorMessage.setIndonesian("Data Tidak Ditemukan");
        errorSchema.setErrorCode("ESB-99-001");
        errorSchema.setErrorMessage(errorMessage);

        return errorSchema;
    }

    public static ErrorSchema setNotAllowedMessage() {
        ErrorSchema errorSchema = ErrorSchema.builder().build();
        ErrorMessage errorMessage = ErrorMessage.builder().build();
        errorMessage.setEnglish("Not Allowed");
        errorMessage.setIndonesian("Tidak Berhak Akses");
        errorSchema.setErrorCode("ESB-99-002");
        errorSchema.setErrorMessage(errorMessage);

        return errorSchema;
    }

    public static ErrorSchema setGeneralErrorMessage() {
        ErrorSchema errorSchema = ErrorSchema.builder().build();
        ErrorMessage errorMessage = ErrorMessage.builder().build();
        errorMessage.setEnglish("Internal Server Error");
        errorMessage.setIndonesian("Sistem mengalami kendala");
        errorSchema.setErrorCode("ESB-99-003");
        errorSchema.setErrorMessage(errorMessage);

        return errorSchema;
    }

    public static ErrorSchema setDuplicateIdMessage() {
        ErrorSchema errorSchema = ErrorSchema.builder().build();
        ErrorMessage errorMessage = ErrorMessage.builder().build();

        errorMessage.setEnglish("Duplicate Key");
        errorMessage.setIndonesian("Duplikat Data");
        errorSchema.setErrorCode("ESB-99-004");
        errorSchema.setErrorMessage(errorMessage);

        return errorSchema;
    }
}
